
var adMinHeight = $('#catfish').height(),
	winH,
	contentH,
	plrContentParts,
	contentOuter = $('#content-outer'),
	topContent,
	ad = $('#ad'),
	catfish = $('#catfish'),
	lastScrollInView = null,
	lastScrollTop,
	catfishInView = true,
	getScrollDirection = () => true;

$(document).ready(init);

function init() {
	plrContentParts = $('.content-wrapper');
	topContent = $(plrContentParts[1]);
	resize();
	onScroll();
	getScrollDirection = (l, c) => l - c > 0;

	$('#content-outer').scroll(onScroll);
}

function onScroll() {

	var y = contentOuter.scrollTop();
	var dir = getScrollDirection(lastScrollTop, y);

	(inView => {

		if (lastScrollInView === false && inView)
			callbacks.adInView();

		lastScrollInView !== false && ({
			true: () => {

				var pixelsInView = y + winH - contentH;
				var percentage = percent(pixelsInView, winH);
				var percentageInView = 100 - Math.abs(percentage);
				var adHeight = percentageInView * winH * .01;
				var adMinY = getPosition(y);
				var adBottom = percentage > 0 ? clamp(adMinY + winH - adHeight, adMinY, 1e10) : clamp(adMinY + winH - adHeight, -1 * 1e10, adMinY);

				if (percentageInView > 20) {
					catfish.css({
						bottom: `${adBottom + ((percentageInView / 100) * topContent.height()) - adMinHeight}px`,
					});

					if (catfishInView) {
						catfishInView = false;
						TweenMax.to('#catfish-img', 0.5, {top: -adMinHeight});
					}
				} else if (percentageInView < 20) {
					if (!catfishInView) {
						catfishInView = true;
						$('#catfish').css({
							bottom: 0
						});
						TweenMax.to('#catfish-img', 0.5, {top: 0});
					}
				}

				scrollCallbackHandler(percentageInView);

				snapToView(percentage, 20, dir, 100);

				if ((dir && y < contentH + winH))
					catfishController.hide(dir);

			},
			false: () => {

				let o = { height: `${adMinHeight}px` };
				o[dir ? 'bottom' : 'top'] = 0;
				o[!dir ? 'bottom' : 'top'] = '';
				catfishController.show(dir);
				catfish.css(o);

				callbacks.adOutOfView(dir);
			}

		})[inView]();

		lastScrollInView = inView;

	})(adInView(y));

	lastScrollTop = y;
}

var catfishController = (() => {

	var goneForever = false;
	var hiddenOnce = false;

	let close = () => {
		goneForever = true;
		catfish.hide();
		callbacks.catfishClosed();
	}

	return {
		close: close,
		show: direction => {
			console.log('trying to show', direction, goneForever)
			if (!goneForever || direction) {
				catfish.show();
				callbacks.catfishShown();
				if (!direction && hiddenOnce)
					setTimeout(close, 2000);
			}
		},
		hide: () => {
			hiddenOnce = true;
			catfish.hide();
			callbacks.catfishHidden();
		}
	};

})();

function snapToView(percentage, scrollPercent, direction, speed) {
	if (percentage <= scrollPercent && percentage > 0 && !direction) {
		contentOuter.animate({
			scrollTop: contentH
		}, speed);

		callbacks.adSnapped();

		catfishController.hide(direction);
	}
}

function clamp(v, min, max) {
	return v < min ? min : v > max ? max : v;
}

function percent(a, b) {
	return ((b - a) / b) * 100;
}

function getPosition(y) {
	return y - contentH;
}

function adInView(y) {
	var adInView = (y + winH >= contentH && y <= contentH + winH);
	return adInView;
}

function resize() {
	winH = window.innerHeight;
	contentH = $(plrContentParts[0]).height();
	$('#adSpace').height(winH);
	$('.content.ad').height(winH);
}

var frame = document.getElementById('adFrame');

var callbacks = {

	//when ad is in view and user scrolls
	//param : percentageInView - how much of the ad is in view (0-100)
	adInViewScroll: percentageInView => { frame.contentWindow.postMessage({ event: 'adInViewScroll', parameters: [percentageInView] }, '*'); },

	//when ad first appears (minified to full)
	adInView: () => { frame.contentWindow.postMessage({ event: 'adInView', parameters: [] }, '*'); },

	//when add is scrolled out of view (minified appears again)
	//param : aboveAd - true if the users viewport is above the main ad unit, false if below
	adOutOfView: aboveAd => { frame.contentWindow.postMessage({ event: 'aboveAd', parameters: [aboveAd] }, '*'); },

	//when the catfish is hidden
	catfishHidden: () => { frame.contentWindow.postMessage({ event: 'catfishHidden', parameters: [] }, '*'); },

	//when the catfish is closed (by timeout) 
	catfishClosed: () => { frame.contentWindow.postMessage({ event: 'catfishClosed', parameters: [] }, '*'); },

	//when the catfish appears after being hidden
	catfishShown: () => { frame.contentWindow.postMessage({ event: 'catfishShown', parameters: [] }, '*'); },

	//when ad snaps to full screen
	adSnapped: () => { frame.contentWindow.postMessage({ event: 'adSnapped', parameters: [] }, '*'); }
};

var scrollCallbackHandler = (() => {
	var lastCallback = 0;
	var callbackPercentages = [25, 50, 75, 100];
	var called = new Array(callbackPercentages.length);

	callbackPercentages.forEach(perc => callbacks[`ad${perc}PercentInView`] = () => {
		frame.contentWindow.postMessage({ event: '`ad${perc}PercentInView`', parameters: [] }, '*');
	});

	return percentageInView => {

		callbacks.adInViewScroll(percentageInView);

		if (percentageInView > lastCallback) {

			callbackPercentages.forEach((perc, i) => {
				if (percentageInView >= perc && called[i] === perc) {
					callbacks[`ad${perc}PercentInView`]();
					lastCallback = perc;
					called[i] = perc;
				}
			});
		}
	}
})();