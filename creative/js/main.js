// JavaScript Document

$(window).load(function(e) {
    animate();
});


var flickrTl = new TimelineMax();
var min = 10,
	max = 30;

function animate() {
	on('adSnapped', function() { console.log('adSnapped') })
	TweenLite.set('#content', {scale: window.innerWidth / $('#content').width(), transformOrigin: '0 0'});

	var wasClicked = true,
		tl = new TimelineMax();
	//var pulseTl = new TimelineMax();
	
	
	/*=====================================================*/
	/* SET STUFF ------------------------------------------*/
	/*=====================================================*/
	
	refreshPositioning();
	
	/*=====================================================*/
	/* TAP HANDLERS ---------------------------------------*/
	/*=====================================================*/
	
	$("#moreContent").click(function(){
		if(wasClicked){
			wasClicked = false;
			
			pulse.kill();
			
			TweenMax.to($("#moreContent"), 1, {width:5, x:-715, onComplete:function(){ TweenMax.set($(".circle"), {rotation:180}); TweenMax.to($(".circle"), 0.5, {x:40})}});
			
			TweenMax.to($(".scene1, .scene2"), 1, {x:-720});
		}
		else{
			wasClicked = true;
			TweenMax.to($("#moreContent"), 1, {x:0, onComplete:function(){ TweenMax.set($(".circle"), {rotation:0}); TweenMax.to($(".circle"), 0.5, {x:0})}});
			
			TweenMax.to($(".scene1, .scene2"), 1, {x:0});
		}			
	});
	
	
	/*=====================================================*/
	/* ANIMATE STUFF --------------------------------------*/
	/*=====================================================*/
	
	returnFlickr(6, 0.05);
	
	var pulse = TweenMax.to($("#moreContent"), 0.5, {yoyo:true, repeat:21, width:15});
	
	tl.from($("#minionT2"), 0.5, {y:100, x:-50, ease:Back.easeOut})
	  .from($("#title, #rope"), 1, {ease:Elastic.easeOut, y:-150}, 0.3)
	  .add(flickrTl)
	  .from($("#title"), 0.3, {rotation:15, transformOrigin:"100% 0"}, "-=0.3")
	  //.from($("#minionT1"), 0.3, {opacity:0, x:50, y:-200})
	  .from($("#date"), 1, {ease:Elastic.easeOut, x:-50, opacity:0})
	  .from($("#minionL2"), 0.7, {rotation:35, x:200}, "-=0.3")
	  .from($("#minionL1"), 0.5, {y:-50, x:-150, opacity:0, rotation:-10}, "-=0.2")
	  .from($("#minionR1"), 0.5, {y:-50, x:195, rotation:10}, "-=0.2")
	  .from($("#moreContent"), 1, {right:-45})
	  .from($("#cta"), 1, {ease:Elastic.easeOut, y:100, opacity:0})
	  .add(pulse)
	
	;
}

function refreshPositioning(){
	$('.center-h').each(function(){
		TweenLite.set(this, {left: ($(this).parent().width() / 2) - ($(this).width() / 2)});
	});

	$('.center-v').each(function(){
		TweenLite.set(this, {top: ($(this).parent().height() / 2) - ($(this).height() / 2)});
	});
}

function returnFlickr(_repeat, _time) {
    for (var i = 0; i<_repeat; i++) {
        flickrTl.append(TweenMax.to($("#title"), _time, {x:getRandomInt(min, max)/5, y:getRandomInt(min, max)/5, yoyo:true, repeat:1}));
    }
    flickrTl.append(TweenMax.to($("#title"), _time, {x:0, y:0}));
}

function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min)) + min;
}