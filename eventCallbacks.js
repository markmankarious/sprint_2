
//usage examples:

//include in creative html file : <script src="eventCallbacks.js"></script>

/*

	on('adInViewScroll', percent => {
		console.log(`user has scrolled, the ad is now ${percent}% in view!`);
	});

	on('ad75PercentInView', () => console.log('Ad is now 75% in view!'));

	on('adSnapped', () => console.log('Ad has just snapped to full screen!'));

*/

var on = (() => {

    var callbacks = {

        //when ad is in view and user scrolls
        //param : percentageInView - how much of the ad is in view (0-100)
        adInViewScroll: percentageInView => { },

        //when ad first appears (minified to full)
        adInView: () => { },

        //when add is scrolled out of view (minified appears again)
        //param : aboveAd - true if the users viewport is above the main ad unit, false if below
        adOutOfView: aboveAd => { },

        catfishHidden: () => { },

        catfishClosed: () => { },

        catfishShown: () => { },

        //when ad snaps to full screen
        adSnapped: () => { }

    };

    [25, 50, 75, 100].forEach(perc => callbacks[`ad${perc}PercentInView`] = () => { });

    window.addEventListener('message', function (event) {
        if (callbacks[event.data.event]) {
            callbacks[event.data.event](...event.data.parameters);
        }
    });

    return (event, callback) => callbacks[event] = callback;
})();